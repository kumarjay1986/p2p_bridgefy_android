package p2p.jaykumar.p2p;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

//import com.bridgefy.sdk.client.Bridgefy;
//import com.bridgefy.sdk.client.BridgefyClient;
//import com.bridgefy.sdk.client.Device;
//import com.bridgefy.sdk.client.Message;
//import com.bridgefy.sdk.client.MessageListener;
//import com.bridgefy.sdk.client.RegistrationListener;
//import com.bridgefy.sdk.client.Session;
//import com.bridgefy.sdk.client.StateListener;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import p2p.jaykumar.p2p.Adapter.ChatAdapter;
import p2p.jaykumar.p2p.Adapter.ChatMessage;
import p2p.jaykumar.p2p.DB.ChatList;
import p2p.jaykumar.p2p.DB.DBHelper;
import p2p.jaykumar.p2p.DB.Staff;

public class MainActivity extends AppCompatActivity {

    TextView textView;
    EditText editText;
    String mystaffId;
    private ListView listView;
    Staff selectedStaff;
    private ArrayList<ChatMessage> chatMessages;
    ArrayList<Staff> staffList = new ArrayList<Staff>();
    ChatAdapter adapter;
    DBHelper mydb;
    boolean newUSerFlag;
//    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
//    private static final int REQUEST_FINE_LOCATION=0;
    private BroadcastReceiver br = new MainActivity.MyBroadcastReceiver();

    private static final SimpleDateFormat formatter =  new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Register broadcast receiver
        IntentFilter filter = new IntentFilter("p2p.jaykumar.p2p.MY_RECIVED_NOTIFICATION");
        this.registerReceiver(br, filter);
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences != null) {
            if (!sharedPreferences.getString("StaffNumber","").toString().isEmpty()) {
                mystaffId = sharedPreferences.getString("StaffNumber","").toString();
            }
        }
        listView = (ListView) findViewById(R.id.list_view);
        chatMessages = new ArrayList<>();
        adapter = new ChatAdapter(chatMessages, this);
        listView.setAdapter(adapter);
        textView = (TextView) findViewById(R.id.text);
        editText = (EditText) findViewById(R.id.edittext);
        //loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION,REQUEST_FINE_LOCATION);
//        Bridgefy.initialize(this, "0d3a6ff1-45ba-4443-8e2a-ef1ee01a01bc", new RegistrationListener() {
//            @Override
//            public void onRegistrationSuccessful(BridgefyClient bridgefyClient) {
//                super.onRegistrationSuccessful(bridgefyClient);
//
//                //Important data can be fetched from the BridgefyClient object
//                //deviceId.setText(bridgefyClient.getUserUuid());
//
//                //Once the registration process has been successful, we can start operations
//                Bridgefy.start(messageListener, stateListener);
//            }
//
//            @Override
//            public void onRegistrationFailed(int i, String s) {
//                super.onRegistrationFailed(i, s);
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        Intent intent = getIntent();
        selectedStaff = (Staff)intent.getParcelableExtra("SELECTEDINDEX");
        staffList = (ArrayList<Staff>)intent.getSerializableExtra("ALLSTAFF");
        newUSerFlag = (boolean)intent.getSerializableExtra("NEWUSERFLAG");
        mydb = new DBHelper(this);
        if (selectedStaff != null){
            final ArrayList<ChatList> chatArrayList = mydb.getAllChatCotacts(selectedStaff.getId());
            for ( ChatList chatList : chatArrayList){
                ChatMessage msg = new ChatMessage();
                if (chatList.getSenderInfo().equals(mystaffId)){
                    msg.setMessageText(chatList.getMessage());
                    msg.setFromText("You");
                    msg.setSenderDate(chatList.getSenderDate());
                    chatMessages.add(msg);
                }
                else
                {
                    msg.setMessageText(chatList.getMessage());
                    msg.setFromText(chatList.getSenderInfo());
                    msg.setSenderDate(chatList.getSenderDate());
                    chatMessages.add(msg);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }
        private void setAdapterForListView(String toName , String message ,String fromName ,String date)
        {
            if (fromName.equals(mystaffId)){
                final ChatMessage msg = new ChatMessage();
                msg.setMessageText(message);
                msg.setFromText("You");
                msg.setSenderDate(date);
                chatMessages.add(msg);
                adapter.notifyDataSetChanged();
            }
            else
            {
                final ChatMessage msg = new ChatMessage();
                msg.setMessageText(message);
                msg.setFromText(fromName);
                msg.setSenderDate(date);
                chatMessages.add(msg);
                adapter.notifyDataSetChanged();
            }
            listView.smoothScrollToPosition(adapter.getCount());
        }

//    public StateListener stateListener = new StateListener() {
//        @Override
//        public void onDeviceConnected(Device device, Session session) {
//            super.onDeviceConnected(device, session);
//            // Do something with the found device
////            HashMap<String ,Object> data = new HashMap<>();
////            data.put("Message" , "Hello World");
////            device.sendMessage(data);
//        }
//
//        @Override
//        public void onDeviceLost(Device device) {
//            super.onDeviceLost(device);
//        }
//    };


    public void clickToSend(View view) {
        Calendar newCalendar = Calendar.getInstance();
        Date now = newCalendar.getTime();
        String date = formatter.format(now);
        HashMap<String ,Object> data = new HashMap<>();
        ChatList chatList = new ChatList();
        Staff staff = new Staff();
        if (newUSerFlag){
            String[] subString =  editText.getText().toString().split("-");
            if (subString.length > 1){
                chatList.setToStaffInfo(subString[0].toString());
                chatList.setMessage(subString[1].toString());
                chatList.setSenderInfo(mystaffId);
                chatList.setSenderDate(date);
                data.put("toStaffName", subString[0].toString());
                data.put("Message" , subString[1].toString());
                data.put("senderInfo" , mystaffId);
                data.put("senderTime" , date);
                staff.setNumber(subString[0].toString());
            }
            else
            {
                chatList.setToStaffInfo(subString[0].toString());
                chatList.setMessage(subString[0].toString());
                chatList.setSenderInfo(mystaffId);
                chatList.setSenderDate(date);
                data.put("toStaffName", subString[0].toString());
                data.put("Message" , subString[1].toString());
                data.put("senderInfo" , mystaffId);
                data.put("senderTime" , date);
                staff.setNumber(subString[0].toString());
            }
            //Load Chat List
            setAdapterForListView(staff.getNumber() ,chatList.getMessage() , mystaffId ,chatList.getSenderDate());
            //Check If Exist In DB
            for (Staff existStaff : staffList)
            {
                    if (existStaff.getNumber().equals(staff.getNumber())){
                        chatList.setStaff(existStaff);
                        mydb.insertChatMessageContact(chatList);
                        editText.setText("");
                        Intent intent = new Intent();
                        intent.setAction("p2p.jaykumar.p2p.MY_NOTIFICATION");
                        intent.putExtra("Message", data);
                        sendBroadcast(intent);

                        return;
                    }
            }
            //Create New Staff
            mydb.insertStaffContact(staff);
            ArrayList<Staff> existingList = mydb.getAllStaffCotacts();
            for (Staff savedstaff : existingList)
            {
                if (!staffList.contains(savedstaff)){
                    staffList.add(savedstaff);
                    chatList.setStaff(savedstaff);
                }
            }
            mydb.insertChatMessageContact(chatList);
            newUSerFlag = false;
        }
        else
        {
            String[] subString =  editText.getText().toString().split("-");
            if (subString.length > 1){
                chatList.setToStaffInfo(selectedStaff.getNumber());
                chatList.setMessage(subString[1].toString());
                chatList.setSenderInfo(mystaffId);
                chatList.setSenderDate(date);
                chatList.setStaff(selectedStaff);
                data.put("toStaffName", selectedStaff.getNumber());
                data.put("Message" , subString[1].toString());
                data.put("senderInfo" , mystaffId);
                data.put("senderTime" , date);
            }
            else
            {
                chatList.setToStaffInfo(selectedStaff.getNumber());
                chatList.setMessage(editText.getText().toString());
                chatList.setSenderInfo(mystaffId);
                chatList.setSenderDate(date);
                chatList.setStaff(selectedStaff);
                data.put("toStaffName", selectedStaff.getNumber());
                data.put("Message" , editText.getText().toString());
                data.put("senderInfo" , mystaffId);
                data.put("senderTime" , date);
            }
            mydb.insertChatMessageContact(chatList);
            setAdapterForListView(selectedStaff.getNumber() ,editText.getText().toString() , mystaffId,chatList.getSenderDate());
        }

        editText.setText("");
        //Message message = Bridgefy.createMessage(null,data);
        Intent intent = new Intent();
        intent.setAction("p2p.jaykumar.p2p.MY_NOTIFICATION");
        intent.putExtra("Message", data);
        sendBroadcast(intent);

       // Bridgefy.sendBroadcastMessage(message);


    }
        //Recive Adapter Lisner
    class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
        Staff NewStaff = (Staff)intent.getParcelableExtra("NewStaff");
            if (!staffList.contains(NewStaff)){
                staffList.add(NewStaff);
            }
            String toString = (String) intent.getSerializableExtra("ToSender");
            String message = (String)intent.getSerializableExtra("Message");
            String senderInfo = (String)intent.getSerializableExtra("SenderInfo");
            String senderdate = (String)intent.getSerializableExtra("senderTime");
            setAdapterForListView(toString ,message, senderInfo,senderdate);
        }
    };

//    MessageListener messageListener = new MessageListener() {
//        @Override
//        public void onMessageReceived(Message message) {
//            // Do something with the received message
//            Log.i("TAG", "message12");
//        }
//        @Override
//        public void onBroadcastMessageReceived(Message message) {
//            // Broadcast messages don't have a Session object attached to them
//            Log.i("TAG", "message123");
//            if (message.getContent().get("toStaffName").toString().equals(mystaffId)) {
//                setAdapterForListView(message.getContent().get("toStaffName").toString() ,message.getContent().get("Message").toString() , message.getContent().get("senderInfo").toString());
//                for (Staff staff : staffList){
//                    if (staff.getNumber().equals(message.getContent().get("senderInfo").toString())){
//                        ChatList chatList = new ChatList();
//                        chatList.setMessage(message.getContent().get("Message").toString());
//                        chatList.setSenderInfo( message.getContent().get("senderInfo").toString());
//                        chatList.setToStaffInfo(message.getContent().get("toStaffName").toString());
//                        chatList.setSenderDate(message.getContent().get("senderTime").toString());
//                        chatList.setStaff(staff);
//                        return;
//                    }
//                }
//
//                Staff staff = new Staff();
//                staff.setNumber(message.getContent().get("senderInfo").toString());
//                mydb.insertStaffContact(staff);
//                ChatList chatList = new ChatList();
//                chatList.setToStaffInfo(message.getContent().get("toStaffName").toString());
//                chatList.setMessage(message.getContent().get("Message").toString());
//                chatList.setSenderInfo(message.getContent().get("senderInfo").toString());
//                chatList.setSenderDate(message.getContent().get("Time").toString());
//                ArrayList<Staff> existingList = mydb.getAllStaffCotacts();
//                int count = mydb.numberOfStaffRows();
//                for (Staff savedstaff : existingList)
//                {
//                    if (!staffList.contains(existingList.get(count - 1))){
//                        staffList.add(savedstaff);
//                    }
//                }
//                chatList.setStaff(existingList.get(count-1));
//                mydb.insertChatMessageContact(chatList);
//            }
//
//        }
//    };
//
//    private void loadPermissions(String perm,int requestCode) {
//        if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
//            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
//                ActivityCompat.requestPermissions(this, new String[]{perm},requestCode);
//            }
//        }
//    }
//    class MyBroadcastReceiver extends BroadcastReceiver {
//        @Override
//        public void onReceive(Context ctxt, Intent intent) {
//            HashMap<String ,Object> data = (HashMap<String ,Object>)intent.getSerializableExtra("Message");
//            //callWhenMessageArrive();
//            System.out.println("received");
//
//        }
//    };

//};

}
