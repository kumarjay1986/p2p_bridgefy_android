package p2p.jaykumar.p2p.DB;

/**
 * Created by jaykumar on 6/11/17.
 */

import android.os.Parcel;
import android.os.Parcelable;
public class Staff implements Parcelable {


    private int id;
    private  String number;

    public Staff(){
        super();
    }
    public Staff(int id, String number){
        super();
        this.id = id;
        this.number = number;

    }
    public Staff(String number){
        this.number = number;
    }
private Staff(Parcel in){
    super();
    this.id = in.readInt();
    this.number = in.readString();
}
public int getId(){
    return id;
}
public void setId(int id){

    this.id = id;
}
public String getNumber(){
    return number;
}
public void setNumber(String number){
    this.number = number;
}
@Override
    public String toString(){
    return "id" + id + ",number:"+ number;
}
@Override
    public int describeContents(){
    return 0;
}
@Override
    public void writeToParcel(Parcel parcel , int flags){
    parcel.writeInt(getId());
    parcel.writeString(getNumber());
}
public static final Parcelable.Creator<Staff> CREATOR = new Parcelable.Creator<Staff>(){
    public Staff createFromParcel(Parcel in){return new Staff(in);}
    public Staff[] newArray(int size){return new Staff[size];}
};
@Override
    public int hashCode(){

    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
}
@Override
    public boolean equals(Object obj){
    if (this == obj)
        return true;
    if (obj == null)
        return  false;
    if (getClass() != obj.getClass())
        return false;
    Staff staff = (Staff) obj;
    if (id != staff.id)
        return false;
    return true;


}
}

