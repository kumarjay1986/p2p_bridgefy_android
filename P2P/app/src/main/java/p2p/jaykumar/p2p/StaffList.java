package p2p.jaykumar.p2p;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.content.Context;




import java.util.ArrayList;
import java.util.HashMap;

import p2p.jaykumar.p2p.Adapter.StaffAdapter;
import p2p.jaykumar.p2p.DB.ChatList;
import p2p.jaykumar.p2p.DB.DBHelper;
import p2p.jaykumar.p2p.DB.Staff;

import com.bridgefy.sdk.client.Bridgefy;
import com.bridgefy.sdk.client.BridgefyClient;
import com.bridgefy.sdk.client.Device;
import com.bridgefy.sdk.client.Message;
import com.bridgefy.sdk.client.MessageListener;
import com.bridgefy.sdk.client.RegistrationListener;
import com.bridgefy.sdk.client.Session;
import com.bridgefy.sdk.client.StateListener;


/**
 * Created by jaykumar on 5/31/17.
 */

public class StaffList extends AppCompatActivity {
    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;
    private static final int REQUEST_FINE_LOCATION=0;
    private ListView listView;
    private FloatingActionButton imageButton;
    DBHelper myDB;
    ArrayList<Staff> staffList = new ArrayList<Staff>();
    private BroadcastReceiver br = new MyBroadcastReceiver();
    StaffAdapter adapter;
    String mystaffId;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.staff_layout);
        loadPermissions(Manifest.permission.ACCESS_FINE_LOCATION,REQUEST_FINE_LOCATION);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (sharedPreferences != null) {
            if (!sharedPreferences.getString("StaffNumber","").toString().isEmpty()) {
                mystaffId = sharedPreferences.getString("StaffNumber","").toString();
            }
        }

        //Register broadcast receiver
        IntentFilter filter = new IntentFilter("p2p.jaykumar.p2p.MY_NOTIFICATION");
        this.registerReceiver(br, filter);
        myDB = new DBHelper(this);

        ArrayList<Staff> availableStaffList = myDB.getAllStaffCotacts();
        for (Staff staff : availableStaffList){
            staffList.add(staff);
        }
        adapter = new StaffAdapter(staffList, this);
        listView =(ListView) findViewById(R.id.StaffList);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Intent chatView = new Intent(view.getContext(),MainActivity.class);
                chatView.putExtra("SELECTEDINDEX" , staffList.get(position));
                chatView.putExtra("ALLSTAFF" , staffList);
                chatView.putExtra("NEWUSERFLAG" , false);
                startActivity(chatView);
            }
        });
        imageButton = (FloatingActionButton)findViewById(R.id.imageButton);
        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent chatView = new Intent(v.getContext(),MainActivity.class);
                chatView.putExtra("ALLSTAFF" , staffList);
                chatView.putExtra("NEWUSERFLAG" , true);
                startActivity(chatView);
            }
        });
        Bridgefy.initialize(getApplicationContext(), new RegistrationListener() {
            @Override
            public void onRegistrationSuccessful(BridgefyClient bridgefyClient) {
                super.onRegistrationSuccessful(bridgefyClient);

                //Important data can be fetched from the BridgefyClient object
                //deviceId.setText(bridgefyClient.getUserUuid());

                //Once the registration process has been successful, we can start operations
                Bridgefy.start(messageListener, stateListener);
            }

            @Override
            public void onRegistrationFailed(int i, String s) {
                super.onRegistrationFailed(i, s);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        ArrayList<Staff> availableStaffList = myDB.getAllStaffCotacts();
        for (Staff staff : availableStaffList){
            if (!staffList.contains(staff)) {
                staffList.add(staff);
            }
        }
        adapter.notifyDataSetChanged();

    }

    public StateListener stateListener = new StateListener() {
        @Override
        public void onDeviceConnected(Device device, Session session) {
            super.onDeviceConnected(device, session);
            // Do something with the found device
//            HashMap<String ,Object> data = new HashMap<>();
//            data.put("Message" , "Hello World");
//            device.sendMessage(data);
        }

        @Override
        public void onDeviceLost(Device device) {
            super.onDeviceLost(device);
        }
    };
    private void loadPermissions(String perm,int requestCode) {
        if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, perm)) {
                ActivityCompat.requestPermissions(this, new String[]{perm},requestCode);
            }
        }
    }


    class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context ctxt, Intent intent) {
            HashMap<String ,Object> data = (HashMap<String ,Object>)intent.getSerializableExtra("Message");
            Message message = Bridgefy.createMessage(null,data);
            Bridgefy.sendBroadcastMessage(message);
            System.out.println("received");

        }
    };

    MessageListener messageListener = new MessageListener() {
        @Override
        public void onMessageReceived(com.bridgefy.sdk.client.Message message) {
            // Do something with the received message
            Log.i("TAG", "message12");
        }
        @Override
        public void onBroadcastMessageReceived(com.bridgefy.sdk.client.Message message) {
            // Broadcast messages don't have a Session object attached to them
            Log.i("TAG", "message123");
            if (message.getContent().get("toStaffName").toString().equals(mystaffId)) {
                //setAdapterForListView(message.getContent().get("toStaffName").toString() ,message.getContent().get("Message").toString() , message.getContent().get("senderInfo").toString());
                for (Staff staff : staffList){
                    if (staff.getNumber().equals(message.getContent().get("senderInfo").toString())){
                        ChatList chatList = new ChatList();
                        chatList.setMessage(message.getContent().get("Message").toString());
                        chatList.setSenderInfo( message.getContent().get("senderInfo").toString());
                        chatList.setToStaffInfo(message.getContent().get("toStaffName").toString());
                        chatList.setSenderDate(message.getContent().get("senderTime").toString());
                        chatList.setStaff(staff);
                        Intent intent = new Intent();
                        intent.setAction("p2p.jaykumar.p2p.MY_RECIVED_NOTIFICATION");
                        intent.putExtra("ToSender", message.getContent().get("toStaffName").toString());
                        intent.putExtra("Message", message.getContent().get("Message").toString());
                        intent.putExtra("SenderInfo", message.getContent().get("senderInfo").toString());
                        intent.putExtra("senderTime", message.getContent().get("senderTime").toString());
                        intent.putExtra("NewStaff", staff);
                        sendBroadcast(intent);
                        return;
                    }
                }
                Staff staff = new Staff();
                staff.setNumber(message.getContent().get("senderInfo").toString());
                myDB.insertStaffContact(staff);
                ChatList chatList = new ChatList();
                chatList.setToStaffInfo(message.getContent().get("toStaffName").toString());
                chatList.setMessage(message.getContent().get("Message").toString());
                chatList.setSenderInfo(message.getContent().get("senderInfo").toString());
                chatList.setSenderDate(message.getContent().get("senderTime").toString());
                ArrayList<Staff> existingList = myDB.getAllStaffCotacts();
                for (Staff savedstaff : existingList)
                {
                    if (!staffList.contains(savedstaff)){
                        staffList.add(savedstaff);
                        adapter.notifyDataSetChanged();
                        chatList.setStaff(savedstaff);
                    }
                }
                myDB.insertChatMessageContact(chatList);
                Intent intent = new Intent();
                intent.setAction("p2p.jaykumar.p2p.MY_RECIVED_NOTIFICATION");
                intent.putExtra("ToSender", message.getContent().get("toStaffName").toString());
                intent.putExtra("Message", message.getContent().get("Message").toString());
                intent.putExtra("SenderInfo", message.getContent().get("senderInfo").toString());
                intent.putExtra("senderTime", message.getContent().get("senderTime").toString());
                intent.putExtra("NewStaff", chatList.getStaff());
                sendBroadcast(intent);
                //adapter.notifyDataSetChanged();
            }

        }

    };

}
