package p2p.jaykumar.p2p.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.BaseAdapter;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import p2p.jaykumar.p2p.DB.Staff;
import p2p.jaykumar.p2p.R;


/**
 * Created by jaykumar on 6/12/17.
 */

public class StaffAdapter extends BaseAdapter {
    private ArrayList<Staff> staffList;
    private Context context;
    public StaffAdapter(ArrayList<Staff> staff, Context context) {
        this.staffList = staff;
        this.context = context;

    }
    @Override
    public int getCount() {
        return staffList.size();
    }

    @Override
    public Object getItem(int position) {
        return staffList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        Staff staff = staffList.get(position);
        StaffAdapter.ViewHolder1 holder1;
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.staff_list_layout, null, false);
                holder1 = new StaffAdapter.ViewHolder1();
                holder1.from = (TextView) v.findViewById(R.id.text1);
                v.setTag(holder1);
            } else {
                v = convertView;
                holder1 = (StaffAdapter.ViewHolder1) v.getTag();

            }
            holder1.from.setText(staff.getNumber());
        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    private class ViewHolder1 {
        public TextView from;
    }


}
