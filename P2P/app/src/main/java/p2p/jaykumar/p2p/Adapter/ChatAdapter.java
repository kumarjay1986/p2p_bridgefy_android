package p2p.jaykumar.p2p.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import p2p.jaykumar.p2p.R;


/**
 * Created by jaykumar on 6/1/17.
 */

public class ChatAdapter extends BaseAdapter {

    private ArrayList<ChatMessage> chatMessages;
    private Context context;
    public ChatAdapter(ArrayList<ChatMessage> chatMessages, Context context) {
        this.chatMessages = chatMessages;
        this.context = context;

    }


    @Override
    public int getCount() {
        return chatMessages.size();
    }

    @Override
    public Object getItem(int position) {
        return chatMessages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = null;
        ChatMessage message = chatMessages.get(position);
        ViewHolder1 holder1;
        ViewHolder2 holder2;

        if (message.getFromText().equals("You")) {
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.list_item_right, null, false);
                holder1 = new ViewHolder1();
                holder1.from = (TextView) v.findViewById(R.id.text1);
                holder1.messageTextView = (TextView) v.findViewById(R.id.text2);
                holder1.date = (TextView) v.findViewById(R.id.text3);
                v.setTag(holder1);
            } else {
                v = convertView;
                holder1 = (ViewHolder1) v.getTag();

            }
            holder1.from.setText(message.getFromText());
            holder1.messageTextView.setText(message.getMessageText());
            holder1.date.setText(message.getSenderDate());

        } else  {
            if (convertView == null) {
                v = LayoutInflater.from(context).inflate(R.layout.list_item, null, false);
                holder2 = new ViewHolder2();

                holder2.from = (TextView) v.findViewById(R.id.text1);
                holder2.messageTextView = (TextView) v.findViewById(R.id.text2);
                holder2.date = (TextView) v.findViewById(R.id.text3);
                v.setTag(holder2);

            } else {
                v = convertView;
                holder2 = (ViewHolder2) v.getTag();

            }
            holder2.from.setText(message.getFromText());
            holder2.messageTextView.setText(message.getMessageText());
            holder2.date.setText(message.getSenderDate());
        }

    
        return v;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }


    private class ViewHolder1 {
        public TextView messageTextView;
        public TextView from;
        public TextView date;
    }

    private class ViewHolder2 {
        public TextView messageTextView;
        public TextView from;
        public TextView date;

    }
}