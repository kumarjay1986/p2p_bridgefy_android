package p2p.jaykumar.p2p;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

/**
 * Created by jaykumar on 6/6/17.
 */

public class login extends AppCompatActivity {


    EditText staffNumber;
    EditText staffName;
    LinearLayout loginView;

    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
       if (sharedPreferences != null) {
           if (!sharedPreferences.getString("StaffNumber","").toString().isEmpty()) {
               Intent chatView = new Intent(this, StaffList.class);
               startActivity(chatView);
               return;
           }
       }
        setContentView(R.layout.login_layout);
        loginView = (LinearLayout)findViewById(R.id.logView);
        staffName = (EditText)findViewById(R.id.staffname);
        staffNumber = (EditText)findViewById(R.id.staffNuber);

    }
    public void clickForLogin(View view) {

        if (staffNumber.getText().toString().trim().length() > 0)
        {
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            SharedPreferences.Editor prefsEditor = sharedPreferences.edit();
            prefsEditor.putString("StaffNumber",staffNumber.getText().toString());
            prefsEditor.commit();
            Intent chatView = new Intent(view.getContext(),StaffList.class);
            startActivity(chatView);
        }
        else{
            Toast.makeText(this, "Please Enter Staff Number",
                    Toast.LENGTH_SHORT).show();


        }

    }





}
