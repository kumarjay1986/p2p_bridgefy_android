package p2p.jaykumar.p2p.Adapter;

/**
 * Created by jaykumar on 6/1/17.
 */

public class ChatMessage {

    private String messageText;
    private String fromText;
    private  String senderDate;
    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }
    public void setFromText(String fromText) {
        this.fromText = fromText;
    }
    public void setSenderDate(String date){this.senderDate = date;}
    public String getMessageText() {
        return messageText;
    }
    public String getFromText() {
        return fromText;
    }
    public String getSenderDate(){return  senderDate;}
}
