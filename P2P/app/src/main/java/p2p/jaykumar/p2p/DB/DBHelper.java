package p2p.jaykumar.p2p.DB;

/**
 * Created by jaykumar on 6/8/17.
 */
import java.util.ArrayList;
import java.util.HashMap;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper{

    public static final String DATABASE_NAME = "ChatHistory.db";
    public static final String STAFF_TABLE_NAME = "StaffList";
    public static final String STAFF_COLUMN_ID = "id";
    public static final String STAFF_COLUMN_NUMBER = "number";
    public static final String STAFF_TABLE_CHATLIST = "ChatList";
    public static final String CHAT_TOSEND = "tosend";
    public static final String CHAT_MESSAGE = "message";
    public static final String CHAT_SENDER = "sender";
    public static final String CHAT_REFRENCEID = "refrence";
    public static final String CHAT_ID = "id";
    public static final String CHAT_TIME = "time";
    private HashMap hp;

    private static final String WHERE_ID_EQUALS = STAFF_COLUMN_ID + "=?";
    private static final String WHERE_CHAT_ID_EQUALS = CHAT_ID + "=?";

    public DBHelper(Context context) {
        super(context, DATABASE_NAME , null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    // TODO Auto-generated method stub
    db.execSQL("CREATE TABLE " + STAFF_TABLE_NAME  +
            "(" + STAFF_COLUMN_ID + " INTEGER PRIMARY KEY, " + STAFF_COLUMN_NUMBER + " TEXT )");
        db.execSQL("CREATE TABLE " + STAFF_TABLE_CHATLIST + "(" + CHAT_ID + " INTEGER PRIMARY KEY, " + CHAT_TOSEND + " TEXT, " + CHAT_MESSAGE + " TEXT, " + CHAT_SENDER + " TEXT, " + CHAT_TIME + " TEXT, " + CHAT_REFRENCEID + " INT, "
                + "FOREIGN KEY(" + CHAT_REFRENCEID + ") REFERENCES "
                + STAFF_TABLE_NAME + "(id) " + ")");
    }

    @Override
    public void onOpen(SQLiteDatabase db) {
        super.onOpen(db);
        if (!db.isReadOnly()) {
            // Enable foreign key constraints
            db.execSQL("PRAGMA foreign_keys=ON;");
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
        db.execSQL("DROP TABLE IF EXISTS" + STAFF_TABLE_NAME );
        db.execSQL("DROP TABLE IF EXISTS" + STAFF_TABLE_CHATLIST );
        onCreate(db);

    }
    public boolean insertStaffContact (Staff staff) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STAFF_COLUMN_NUMBER, staff.getNumber());
        db.insert(STAFF_TABLE_NAME, null, contentValues);
        return true;
    }

    public boolean insertChatMessageContact(ChatList chatList){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(CHAT_TOSEND, chatList.getToStaffInfo());
        contentValues.put(CHAT_MESSAGE, chatList.getMessage());
        contentValues.put(CHAT_SENDER, chatList.getSenderInfo());
        contentValues.put(CHAT_TIME, chatList.getSenderDate());
        contentValues.put(CHAT_REFRENCEID , chatList.getStaff().getId());
        db.insert(STAFF_TABLE_CHATLIST, null, contentValues);
        return true;
    }


    public Cursor getStaffData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from" + STAFF_TABLE_NAME +"where id="+id+"", null );
        return res;
    }
    public Cursor getChatListData(int id) {
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from" + STAFF_TABLE_CHATLIST + "where id="+id+"", null );
        return res;
    }

    public int numberOfStaffRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, STAFF_TABLE_NAME);
        return numRows;
    }
    public int numberOfChatRows(){
        SQLiteDatabase db = this.getReadableDatabase();
        int numRows = (int) DatabaseUtils.queryNumEntries(db, STAFF_TABLE_CHATLIST);
        return numRows;
    }
    public boolean updateSelectedStaffContact (Staff staff) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STAFF_TABLE_NAME, staff.getNumber());
        long result = db.update(STAFF_TABLE_NAME,contentValues,WHERE_ID_EQUALS,new String[] { String.valueOf(staff.getId()) });
        Log.d("Updated Result :" ,"=" + result);
//        db.update(STAFF_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public boolean updateSelectedChatContact (ChatList chatList) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(STAFF_TABLE_CHATLIST, chatList.getToStaffInfo());
        contentValues.put(STAFF_TABLE_CHATLIST, chatList.getMessage());
        contentValues.put(STAFF_TABLE_CHATLIST, chatList.getSenderInfo());
        contentValues.put(STAFF_TABLE_CHATLIST, chatList.getSenderDate());

        long result = db.update(STAFF_TABLE_CHATLIST,contentValues,WHERE_ID_EQUALS,new String[] { String.valueOf(chatList.getId()) });
        Log.d("Updated Result :" ,"=" + result);
//        db.update(STAFF_TABLE_NAME, contentValues, "id = ? ", new String[] { Integer.toString(id) } );
        return true;
    }

    public int  deleteSelectedStaffContact (Staff   staff) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(STAFF_TABLE_NAME,
                WHERE_ID_EQUALS, new String[] { staff.getId() + "" });
    }
    public int  deleteSelectedChatContact (ChatList   chatList) {
        SQLiteDatabase db = this.getWritableDatabase();
        return db.delete(STAFF_TABLE_CHATLIST,
                WHERE_ID_EQUALS, new String[] { chatList.getId() + "" });
    }
    public ArrayList<Staff> getAllStaffCotacts() {
        ArrayList<Staff> array_list = new ArrayList<Staff>();
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor res =  db.rawQuery( "select * from " + STAFF_TABLE_NAME, null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            Staff staff = new Staff();
            staff.setId(res.getInt(0));
            staff.setNumber(res.getString(1));
            array_list.add(staff);
            res.moveToNext();
        }
        return array_list;
    }
    public ArrayList<ChatList> getAllChatCotacts(int selectedId) {
        ArrayList<ChatList> array_list = new ArrayList<ChatList>();
        SQLiteDatabase db = this.getReadableDatabase();
        String query = "select * from " + STAFF_TABLE_CHATLIST + " where " + CHAT_REFRENCEID + " = " + selectedId;
        Cursor res =  db.rawQuery( query , null );
        res.moveToFirst();
        while(res.isAfterLast() == false){
            ChatList chatList = new ChatList();
            chatList.setId(res.getInt(0));
            chatList.setToStaffInfo(res.getString(1));
            chatList.setMessage(res.getString(2));
            chatList.setSenderInfo(res.getString(3));
            chatList.setSenderDate(res.getString(4));
            array_list.add(chatList);
            res.moveToNext();
        }
        return array_list;
    }
}
