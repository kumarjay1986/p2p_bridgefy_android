package p2p.jaykumar.p2p.DB;

/**
 * Created by jaykumar on 6/11/17.
 */


import android.os.Parcel;
import android.os.Parcelable;

public class ChatList implements Parcelable {

    private int id;
    private String senderInfo;
    private String toStaffInfo;
    private String message;
    private String senderDate;
    private Staff staff;

    public ChatList() {
        super();
    }

    private ChatList(Parcel in) {
        super();
        this.id = in.readInt();
        this.senderInfo = in.readString();
        this.message = in.readString();
        this.toStaffInfo = in.readString();
        this.senderDate = in.readString();
        this.staff = in.readParcelable(Staff.class.getClassLoader());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSenderInfo() {
        return senderInfo;
    }

    public void setSenderInfo(String senderInfo) {
        this.senderInfo = senderInfo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToStaffInfo() {
        return toStaffInfo;
    }

    public void setToStaffInfo(String toStaffInfo) {
        this.toStaffInfo = toStaffInfo;
    }

    public String getSenderDate() {
        return senderDate;
    }

    public void setSenderDate(String senderDate) {
        this.senderDate = senderDate;
    }

    public Staff getStaff() {
        return staff;
    }

    public void setStaff(Staff staff) {
        this.staff = staff;
    }

    @Override
    public String toString() {
        return "ChatList [id=" + id + ",senderInfo =" + senderInfo + ",toStaffInfo =" + toStaffInfo + ",message =" + message + ",senderDate =" + senderDate + "]";

    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + id;
        return result;

    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ChatList chatList = (ChatList) obj;
        if (id != chatList.id)
            return false;
        return true;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeInt(getId());
        parcel.writeString(getSenderInfo());
        parcel.writeString(getMessage());
        parcel.writeString(getToStaffInfo());
        parcel.writeString(getSenderDate());
        parcel.writeParcelable(getStaff(), flags);
    }

    public static final Parcelable.Creator<ChatList> CREATOR = new Parcelable.Creator<ChatList>() {
        public ChatList createFromParcel(Parcel in) {
            return new ChatList(in);
        }

        public ChatList[] newArray(int size) {
            return new ChatList[size];
        }

    };
}


